Name:      perl-CPAN-Meta
Version:   2.150010
Release:   422
Summary:   The distribution metadata for a CPAN dist
License:   GPL-1.0-or-later OR Artistic-1.0-Perl
URL:       https://metacpan.org/release/CPAN-Meta
Source0:   https://cpan.metacpan.org/authors/id/D/DA/DAGOLDEN/CPAN-Meta-%{version}.tar.gz
BuildArch: noarch

BuildRequires: perl-interpreter perl-generators perl(ExtUtils::MakeMaker) >= 6.17 perl(Test::More) >= 0.88 coreutils findutils 
Requires:      perl(CPAN::Meta::YAML) >= 0.011 perl(Encode) perl(JSON::PP) >= 2.27300 perl(version) >= 0.88

Obsoletes:      perl-Parse-CPAN-Meta < 1:1.4422-2

%{?perl_default_filter}

%description
Software distributions released to the CPAN include a META.json or, 
for older distributions, META.yml, which describes the distribution, 
its contents, and the requirements for building and installing the distribution. 
The data structure stored in the META.json file is described in CPAN::Meta::Spec.

CPAN::Meta provides a simple class to represent this distribution metadata (or distmeta), 
along with some helpful methods for interrogating that data.

%package_help

%prep
%autosetup -n CPAN-Meta-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PERLLOCAL=1 NO_PACKLIST=1
%make_build

%install
%make_install 
%{_fixperms} %{buildroot}/*

%check
make test

%files
%license LICENSE
%doc Changes history README t/
%{perl_vendorlib}/*

%files help
%doc CONTRIBUTING.mkdn Todo
%{_mandir}/*/*

%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 2.150010-422
- drop useless perl(:MODULE_COMPAT) requirement

* Thu Aug 1 2024 yanglongkang <yanglongkang@h-partners.com> - 2.150010-421
- rebuild for next release

* Tue Oct 25 2022 renhongxun <renhongxun@h-partners.com> - 2.150010-420
- Rebuild for next release

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.150010-419
- Change mod of file

* Mon Sep 2 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.150010-418 
- Package init
